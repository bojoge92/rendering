#include <stdio.h>
#include <string.h>
#include <cv.h>
#include <highgui.h>
#include "com_render_jni_Text.h"


JNIEXPORT void JNICALL Java_com_render_jni_Text_text(JNIEnv *env, jobject obj, jstring jFileName, jstring jText,
jint jWidth, jint jHeight, jint jSize){
	
	
	const char* fileName = env->GetStringUTFChars(jFileName, 0);
	const char* text = env->GetStringUTFChars(jText, 0);
	int width = jWidth;
	int height = jHeight;
	int size = jSize;
	
	IplImage *img = cvLoadImage(fileName);
	
	/* initialize font and add text */
	
	
	CvFont font;
	
	cvInitFont(&font,CV_FONT_HERSHEY_PLAIN, 1.0f, 1.0f, 0, 4);

	cvInitFont(&font, CV_FONT_HERSHEY_PLAIN, size/4.0, size/4.0, 0, 4);

	cvPutText(img, text, cvPoint(width, height), &font, CV_RGB(255,255,255));

	/*
	cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 1.5, 1.5, 0, 2, CV_AA);
	cvPutText(img, text, cvPoint(width, height), &font, cvScalar(255, 255, 255, 0));
	
	cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 1.5, 1.5, 0, 1.5, CV_AA);
	cvPutText(img, text, cvPoint(width, height), &font, cvScalar(0, 0, 0, 0));
	*/

	/* save the image */

	cvSaveImage(fileName, img);

	cvReleaseImage( &img );
	
	
}

