
#include <cv.h>
#include <highgui.h>
#include "com_render_jni_Mosaic.h"

IplImage* img; 
IplImage* zoomin,*zoomout;   

JNIEXPORT void JNICALL Java_com_render_jni_Mosaic_mosaic(JNIEnv *env, jobject obj, jstring fileName){


	char c;  
	const char* str = env->GetStringUTFChars(fileName, 0);
	
	img = cvLoadImage(str); 
	zoomin = cvCreateImage(cvSize(img->width/20,img->height/20),IPL_DEPTH_8U,3); 
	zoomout = cvCreateImage(cvSize(img->width,img->height),IPL_DEPTH_8U,3); 

	cvResize(img,zoomin,CV_INTER_NN); 
	cvResize(zoomin,zoomout,CV_INTER_NN);	
	
	cvSaveImage(str, zoomout);
	
	cvReleaseImage(&img); 
	cvReleaseImage(&zoomin); 
	cvReleaseImage(&zoomout);
	
}  
