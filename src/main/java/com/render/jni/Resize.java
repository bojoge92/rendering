package com.render.jni;

public class Resize {
	
	public native void resize(String fileName, int width, int height);

	static {
		System.loadLibrary("resize");
	}
	
	public static void main(String args[]) {
		long start = System.currentTimeMillis();
		
		Resize r = new Resize();
		r.resize("/home/hadoop/hadoop/yarn-beginners-examples-master/src/main/java/11.jpg", 720, 480);
		
		long end = System.currentTimeMillis();

		System.out.println( "실행 시간 : " + ( end - start )/1000.0 ); 
		
	}

}
