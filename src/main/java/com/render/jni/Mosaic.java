package com.render.jni;

public class Mosaic {
	public native void mosaic(String fileName);

	static {
		System.loadLibrary("mosaic");
	}
	
	public static void main(String args[]) {
		Mosaic r = new Mosaic();
		r.mosaic("/home/hadoop/hadoop/yarn-beginners-examples-master/src/main/java/11.jpg");		
	}
}
