package com.render.jni;

public class Blur {
	public native void blur(String fileName);

	static {
		System.loadLibrary("blur");
	}
	
	public static void main(String args[]) {
		Blur r = new Blur();
		r.blur("/home/hadoop/hadoop/yarn-beginners-examples-master/src/main/java/11.jpg");		
	}

}
