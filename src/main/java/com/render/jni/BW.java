package com.render.jni;

public class BW {
	public native void bw(String fileName);

	static {
		System.loadLibrary("bw");
	}
	
	public static void main(String args[]) {
		BW bw = new BW();
		bw.bw("/home/hadoop/hadoop/yarn-beginners-examples-master/src/main/java/11.jpg");		
	}

}
