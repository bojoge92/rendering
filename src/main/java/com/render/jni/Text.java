package com.render.jni;

public class Text {
	public native void text(String fileName, String text, int width, int height, int size);

	static {
		System.loadLibrary("text");
	}
	
	public static void main(String args[]) {
		Text t = new Text();
		t.text("/home/hadoop/hadoop/yarn-beginners-examples-master/src/main/java/11.jpg", "This This ~~", 50, 50, 14);		
	}

}
