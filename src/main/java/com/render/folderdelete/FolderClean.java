package com.render.folderdelete;

import java.io.File;

import com.render.filereceiver.FilePath;

public class FolderClean {

	public static void clean(String ID) {

		/* 폴더 없으면 생성 */
		File dir = new File(FilePath.OUTPUTPATH + ID);
		// 폴더가 없으면 생성
		if (!dir.exists()) {
			dir.mkdir();
		} else {
			// 있다면 현재 디렉토리 파일을 삭제
			deleteDirectory(dir);
			dir.mkdir();
		}

		/* 이미지 분할 임시폴더 폴더 없으면 생성, 있으면 비우기 */
		dir = new File(FilePath.OUTPUTPATH + ID + "/imageTemp");
		// 폴더가 없으면 생성
		if (!dir.exists()) {
			dir.mkdir();
		}

		/* 이미지 분할후 하나로 모을 임시폴더 폴더 없으면 생성, 있으면 비우기 */
		dir = new File(FilePath.OUTPUTPATH + ID + "/imageTemp/All");
		// 폴더가 없으면 생성
		if (!dir.exists()) {
			dir.mkdir();
		}
		
		/* 사운드 임시폴더 폴더 없으면 생성, 있으면 비우기 */
		dir = new File(FilePath.OUTPUTPATH + ID + "/audioTemp");
		// 폴더가 없으면 생성
		if (!dir.exists()) {
			dir.mkdir();
		}

		/* 원본 동영상 폴더 없으면 생성, 있으면 비우기 */
		dir = new File(FilePath.OUTPUTPATH + ID + "/videoTemp");
		// 폴더가 없으면 생성
		if (!dir.exists()) {
			dir.mkdir();
		}
		
		/* 수정된 동영상, 음성 파일 임시 폴더 없으면 생성, 있으면 비우기 */
		dir = new File(FilePath.OUTPUTPATH + ID + "/resultTemp");
		// 폴더가 없으면 생성
		if (!dir.exists()) {
			dir.mkdir();
		}
		
		/*
		 * 결과 다운로드 폴더 없으면 생성 dir = new File(FilePath.OUTPUTPATH + ID +
		 * "/download"); // 폴더가 없으면 생성 if (!dir.exists()) { dir.mkdir(); }
		 */

	}

	public static boolean deleteDirectory(File path) {
		if (!path.exists()) {
			return false;
		}

		File[] files = path.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				deleteDirectory(file);
			} else {
				file.delete();
			}
		}

		return path.delete();
	}

}
