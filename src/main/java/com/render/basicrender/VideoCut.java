package com.render.basicrender;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import com.render.filereceiver.FilePath;
import com.xuggle.mediatool.MediaListenerAdapter;

public class VideoCut extends MediaListenerAdapter {

	// Devo utilizzare diversi writer, uno per ogni parte del filmato da
	// tagliare
	
	
	public void VideoCut(String fileName, String ID, long starts, long ends, int num) {

		String input = FilePath.OUTPUTPATH + ID + "/videoTemp/" + fileName;

		String fileFormat = fileName.substring(fileName.lastIndexOf('.'), fileName.length());		
		
		String output = FilePath.OUTPUTPATH + ID + "/videoTemp_cut/" + num + fileFormat;
		String command = "/home/hadoop/bin/ffmpeg -i " + input + " -ss " + starts + " -t " + ends + " -acodec copy -vcodec copy " + output;
		
		try {
			shellCmd(command);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static void shellCmd(String command) throws Exception {
		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(command);
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
	}
		
	
	/*
	public void VideoCut(String fileName, String ID, long starts, long ends) {
		IMediaWriter writers;
		String TMP_DIR;
		
		IMediaReader reader = ToolFactory.makeReader(FilePath.OUTPUTPATH + ID + "/videoTemp/" + fileName);
		reader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);

		TMP_DIR = FilePath.OUTPUTPATH + ID + "/videoTemp_cut";
		File tmpdir = new File(TMP_DIR);
		tmpdir.mkdir(); // creo una cartella temporanea per salvere i frammenti
		// passo da secondi a nanosecondi
		starts *= Global.DEFAULT_PTS_PER_SECOND;
		ends *= Global.DEFAULT_PTS_PER_SECOND;
		writers = ToolFactory.makeWriter(FilePath.OUTPUTPATH + ID + "/videoTemp_cut/" + starts + ".mov", reader); // comprende
		// il
		// nome
		// del
		// file

		// creazione di un tool che mi taglia il video nei punti scelti
		videoCheck checkPos = new videoCheck(); // videocheck estende
												// MediaToolAdapter
		reader.addListener(checkPos);

		boolean updatedS = false;
		boolean updatedE = false;

		
		File f = new File(FilePath.OUTPUTPATH + ID + "/videoTemp/" + fileName);
		System.out.println(f.length());
		
		
		// reader
		while (reader.readPacket() == null) {
			
			if (!updatedS && (checkPos.timeInMilisec >= starts)) {
				updatedS = true; // da un certo punto inizio a convertire
				updatedE = false;
				checkPos.addListener(writers);
			}

			if (!updatedE && (checkPos.timeInMilisec >= ends)) {
				updatedE = true; // arrivato ad un certo punto smetto di
									// convertire
				checkPos.removeListener(writers);
				writers.close();
				break;
			}
		
		}
		
		System.out.println("end!!");
		
	}

	class videoCheck extends MediaToolAdapter {
		// Devono essere millisecondi
		public Long timeInMilisec = (long) 0;
		public boolean convert = true;

		@Override
		public void onVideoPicture(IVideoPictureEvent event) {
			timeInMilisec = event.getTimeStamp(); // mi ritorna il preciso
													// istante in MICROsecondi
			// adesso chiamo la superclasse che continua con la manipolazione

			if (convert)
				super.onVideoPicture(event);
		}

		@Override
		public void onAudioSamples(IAudioSamplesEvent event) {
			if (convert)
				super.onAudioSamples(event);
		}

		@Override
		public void onWritePacket(IWritePacketEvent event) {
			if (convert)
				super.onWritePacket(event);
		}

		@Override
		public void onWriteTrailer(IWriteTrailerEvent event) {
			if (convert)
				super.onWriteTrailer(event);
		}

		@Override
		public void onReadPacket(IReadPacketEvent event) {
			if (convert)
				super.onReadPacket(event);
		}

		@Override
		public void onWriteHeader(IWriteHeaderEvent event) {
			if (convert)
				super.onWriteHeader(event);
		}
	}
	*/

}
