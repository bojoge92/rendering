package com.render.basicrender;

import java.io.File;

import com.render.filereceiver.FilePath;

public class VideoMergeSort {
	
	public static void mergeSort(String ID, int fileNum){
		
		String path = FilePath.OUTPUTPATH + ID + "/imageTemp/All/";//통합할 폴더
		int i = 0;
		
		for(int folderCount=0; folderCount<fileNum; folderCount++){
		
			File folder = new File(FilePath.OUTPUTPATH + ID + "/imageTemp/" + folderCount);//imaga frame path
			File[] listOfFiles = folder.listFiles();
			
			for (int j=0; j<listOfFiles.length; j++) {
				File fromFile = new File(FilePath.OUTPUTPATH + ID + "/imageTemp/" + folderCount + "/" + String.format("%010d", j+1) + ".jpg");//move to image file
				File toFile = new File(path + String.format("%010d", i++) + ".jpg");
				fromFile.renameTo(toFile);
			}
			
		}
		
	}

}
