package com.render.basicrender;

import java.io.File;

import com.render.filereceiver.FilePath;

public class SplitFrameFromVideo {
	
	
	
	String fileName;
	String ID;
	String fileNum;
	long resolution;
	long starts;
	long ends;
	
	public SplitFrameFromVideo(String fileName, String ID, String fileNum, long resolution, long starts, long ends){
		this.fileName = fileName;
		this.ID = ID;
		this.fileNum = fileNum;
		this.resolution = resolution;
		this.starts = starts;
		this.ends = ends;
	}
	
	public void run(){
		startSplit(fileName, ID, fileNum, resolution, starts, ends);
	}

	public void startSplit(String fileName, String ID, String fileNum, long resolution, long starts, long ends) {
		
		String input = FilePath.OUTPUTPATH + ID + "/videoTemp/" + fileName;
		
		String output = FilePath.OUTPUTPATH + ID + "/imageTemp/" + fileNum;
		
		File mkd = new File(output);//각 분할영상 별 디렉터리 생성
		mkd.mkdirs();
		
		String command = "/home/hadoop/bin/ffmpeg -i " + input + " -ss " + starts + " -t " + ends + " -qscale:v 2 -r " + resolution
				+ " -f image2 " + output + "/%010d.jpg";
		
		shellCmd(command);
		
	}
	
	public void shellCmd(String command) {
		try {
			Runtime runtime = Runtime.getRuntime();
			Process process;
			process = runtime.exec(command);
			process.waitFor();
			process.destroy();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * public static void startSplit(String fileName, String ID, long numbering,
	 * long resolution) {
	 * 
	 * 
	 * IMediaReader mediaReader = ToolFactory.makeReader(FilePath.OUTPUTPATH +
	 * ID + "/videoTemp_cut/" + fileName);
	 * 
	 * // stipulate that we want BufferedImages created in BGR 24bit color //
	 * space mediaReader
	 * .setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);
	 * 
	 * mediaReader.addListener(new ImageSnapListener(ID, numbering,
	 * resolution));
	 * 
	 * // read out the contents of the media file and // dispatch events to the
	 * attached listener while (mediaReader.readPacket() == null) ;
	 * 
	 * }
	 * 
	 * private static class ImageSnapListener extends MediaListenerAdapter {
	 * 
	 * String ID; long numbering; long resolution;
	 * 
	 * // The video stream index, used to ensure we display frames from one and
	 * // only one video stream from the media container. private int
	 * mVideoStreamIndex = -1;
	 * 
	 * // Time of last frame write private long mLastPtsWrite = Global.NO_PTS;
	 * 
	 * public ImageSnapListener(String ID, long numbering, long resolution) {
	 * this.ID = ID; this.resolution = resolution; this.numbering = numbering; }
	 * 
	 * public void onVideoPicture(IVideoPictureEvent event) {
	 * 
	 * if (event.getStreamIndex() != mVideoStreamIndex) { // if the selected
	 * video stream id is not yet set, go ahead an // select this lucky video
	 * stream if (mVideoStreamIndex == -1) mVideoStreamIndex =
	 * event.getStreamIndex(); // no need to show frames from this video stream
	 * else return; }
	 * 
	 * // if uninitialized, back date mLastPtsWrite to get the very first //
	 * frame if (mLastPtsWrite == Global.NO_PTS) mLastPtsWrite =
	 * event.getTimeStamp() - resolution;
	 * 
	 * // if it's time to write the next frame if (event.getTimeStamp() -
	 * mLastPtsWrite >= resolution) {
	 * 
	 * String outputFilename = dumpImageToFile(event.getImage());
	 * 
	 * // indicate file written double seconds = ((double) event.getTimeStamp())
	 * / Global.DEFAULT_PTS_PER_SECOND; // System.out.printf( //
	 * "at elapsed time of %6.3f seconds wrote: %s\n", // seconds,
	 * outputFilename);
	 * 
	 * // update last write time mLastPtsWrite += resolution; }
	 * 
	 * }
	 * 
	 * private String dumpImageToFile(BufferedImage image) {
	 * 
	 * try { String outputFilename = FilePath.OUTPUTPATH + ID + "/imageTemp/" +
	 * String.format("%010d", numbering) + ".jpg"; numbering++;
	 * 
	 * ImageIO.write(image, "jpg", new File(outputFilename)); return
	 * outputFilename; } catch (IOException e) { e.printStackTrace(); return
	 * null; } }
	 * 
	 * }
	 */
}