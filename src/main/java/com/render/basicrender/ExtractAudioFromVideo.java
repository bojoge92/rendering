package com.render.basicrender;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import com.render.filereceiver.FilePath;

public class ExtractAudioFromVideo {
		
	public void soundExtract(String fileName, String ID, int fileNum, long starts, long ends){
		
		String input = FilePath.OUTPUTPATH + ID + "/videoTemp/" + fileName;
		
		String temp = fileNum + ".wav";
		String output = FilePath.OUTPUTPATH + ID + "/audioTemp/" + temp;
		
		String command = "/home/hadoop/bin/ffmpeg -i " + input + " -ss " + starts + " -t " + ends + " " + output;
		// 이 부분에 실행할 리눅스 Shell 명령어를 입력하면 된다. (여기선 ls -al 명령어 입력)
		try {
			shellCmd(command);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void shellCmd(String command) throws Exception {
		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(command);
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		process.waitFor();
		process.destroy();
	}
		
	
	/*
	public static void soundExtract(String input, String ID)
			throws IllegalArgumentException, InputFormatException,
			EncoderException {

		File source = new File(FilePath.OUTPUTPATH + ID + "/videoTemp_cut/" + input);
		
		String temp = input.substring(0, input.lastIndexOf('.')) + ".wav";
		File target = new File(FilePath.OUTPUTPATH + ID + "/audioTemp/" + temp);
		
		
		AudioAttributes audio = new AudioAttributes();
		//audio.setCodec(AudioAttributes.DIRECT_STREAM_COPY);
		EncodingAttributes attrs = new EncodingAttributes();
		attrs.setFormat("wav");
		attrs.setAudioAttributes(audio);
		Encoder encoder = new Encoder();
		encoder.encode(source, target, attrs);
				
		
	}
	 */
	
}
