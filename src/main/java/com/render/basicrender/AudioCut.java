package com.render.basicrender;

import java.io.*;
import javax.sound.sampled.*;

import com.render.filereceiver.FilePath;

public class AudioCut {

	public static void copyAudio(String sourceFileName, String destinationFileName, String id, long startSecond,
			long secondsToCopy) {
		AudioInputStream inputStream = null;
		AudioInputStream shortenedStream = null;
		try {
			File file = new File(FilePath.OUTPUTPATH + id + "/videoTemp/" + sourceFileName);
			AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(file);
			AudioFormat format = fileFormat.getFormat();
			inputStream = AudioSystem.getAudioInputStream(file);
			int bytesPerSecond = format.getFrameSize() * (int) format.getFrameRate();
			inputStream.skip(startSecond * bytesPerSecond);
			long framesOfAudioToCopy = secondsToCopy * (int) format.getFrameRate();
			shortenedStream = new AudioInputStream(inputStream, format, framesOfAudioToCopy);
			File destinationFile = new File(FilePath.OUTPUTPATH + id + "/audioTemp/" + destinationFileName + ".wav");
			AudioSystem.write(shortenedStream, fileFormat.getType(), destinationFile);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null)
				try {
					inputStream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			if (shortenedStream != null)
				try {
					shortenedStream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	}
}