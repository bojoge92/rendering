package com.render.basicrender;

import java.io.File;
import java.io.SequenceInputStream;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import com.render.filereceiver.FilePath;

public class AudioConcat {

	public static void concatAudio(int num, String id) {

		int i;
		
		if(num==1){//합칠게 없다면 그대로 옮김
			File fromFile = new File(FilePath.OUTPUTPATH + id+ "/audioTemp/0.wav");//move to output movie
			File toFile = new File(FilePath.OUTPUTPATH + id + "/resultTemp/audio.wav");//transfer web server PC
			boolean result = fromFile.renameTo(toFile);
		}
		/** 합칠게 두 개 이상일 때 */
		else{			
			String wavFile1 = FilePath.OUTPUTPATH + id+ "/audioTemp/0.wav";//처음은 0과 1 두 개
			String wavFile2 = FilePath.OUTPUTPATH + id+ "/audioTemp/1.wav";
			
			String resultWav = FilePath.OUTPUTPATH + id+ "/audioTemp/result.wav";//결과파일 이름
			
			//0과 1 합쳐서 result.wav로 저장
			try {
				AudioInputStream clip1 = AudioSystem.getAudioInputStream(new File(wavFile1));
				AudioInputStream clip2 = AudioSystem.getAudioInputStream(new File(wavFile2));
				
				AudioInputStream appendedFiles = new AudioInputStream(new SequenceInputStream(clip1, clip2),
						clip1.getFormat(), clip1.getFrameLength() + clip2.getFrameLength());
				
				AudioSystem.write(appendedFiles, AudioFileFormat.Type.WAVE, new File(resultWav));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//영상이 3개 이상일 때 추가 작업
			for (i = 2; i < num; i++) {
				File fromFile = new File(resultWav);//합쳐진 result 파일과 다음 파일 합쳐야 하므로 result 파일명 temp로 변경
				File toFile = new File(FilePath.OUTPUTPATH + id + "/audioTemp/temp.wav");
				if(toFile.exists())//temp파일 존재하면 삭제후 이름 변경
					toFile.delete();
				boolean result = fromFile.renameTo(toFile);
				
				//앞서 합쳐진 파일과 새파일 합침
				wavFile1 = FilePath.OUTPUTPATH + id+ "/audioTemp/temp.wav";
				wavFile2 = FilePath.OUTPUTPATH + id+ "/audioTemp/" + i + ".wav";
	
				try {
					AudioInputStream clip1 = AudioSystem.getAudioInputStream(new File(wavFile1));
					AudioInputStream clip2 = AudioSystem.getAudioInputStream(new File(wavFile2));
					
					AudioInputStream appendedFiles = new AudioInputStream(new SequenceInputStream(clip1, clip2),
							clip1.getFormat(), clip1.getFrameLength() + clip2.getFrameLength());
					
					AudioSystem.write(appendedFiles, AudioFileFormat.Type.WAVE, new File(resultWav));//결과는 result로 저장
				} catch (Exception e) {
					e.printStackTrace();
				}			
				
			}
			
			File fromFile = new File(resultWav);//결과 result 파일 디렉터리 변경
			File toFile = new File(FilePath.OUTPUTPATH + id + "/resultTemp/audio.wav");
			boolean result = fromFile.renameTo(toFile);
			//System.out.println("이동 성공여부 : " + result);		
		}
		
	}
}