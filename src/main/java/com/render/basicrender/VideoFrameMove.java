package com.render.basicrender;

import java.io.File;

import com.render.filereceiver.FilePath;

public class VideoFrameMove {
	
	public static void videoCut(String fileName, String ID, long startTime, long endTime, long numbering, long frame){
		
		long numTemp = numbering;
		
		System.out.println(fileName + " " + ID + " " + startTime + " " + endTime + " " + numbering + " " + frame);
		
		for(long i=startTime*frame; i<endTime*frame; i++){
			
			File fromFile = new File(FilePath.OUTPUTPATH + ID + "/imageTemp_" + fileName + "/" + String.format("%010d", i) + ".jpg");//move to output movie
			File toFile = new File(FilePath.OUTPUTPATH + ID + "/imageTemp/" + String.format("%010d", numTemp) + ".jpg");//transfer web server PC
			boolean result = fromFile.renameTo(toFile);
			numTemp++;
			
		}		
		
	}
	
}
