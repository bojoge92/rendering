package com.render.basicrender;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.media.*;
import javax.media.control.TrackControl;
import javax.media.datasink.*;
import javax.media.format.*;
import javax.media.protocol.*;

import com.render.filereceiver.FilePath;

public class MergeAudioVideo implements ControllerListener, DataSinkListener{

	
	//ffmpeg -i video.mp4 -i audio.wav -c:v copy -c:a aac -strict experimental output.mp4
	
	//ffmpeg  -i videofinal.mp4 -i audiofinal.mp3 -shortest final.mp4
	//ffmpeg -i /tmp/combineAudio.ogg -i /original/test.mov -acodec copy -vcodec copy /tmp/test.mov
	//ffmpeg -i video.avi -i audio.mp3 -codec copy -shortest output.avi
	//ffmpeg -i video.mp4 -i audio.m4a -c:v copy -c:a copy output.mp4
	//ffmpeg -i audioInput.mp3 -i videoInput.avi -acodec copy -vcodec copy outputFile.avi 
	
	
	public void mergeFiles2(String ID) {

		String path = FilePath.OUTPUTPATH + ID + "/resultTemp/";
		
		//String command = "/home/hadoop/bin/ffmpeg -i " + path + "video.mov -i " + path + "audio.wav -c:v copy -c:a copy -strict experimental " + path + "result.mov";
		
		String command = "/home/hadoop/bin/ffmpeg -i " + path + "video.mov -i " + path + "audio.wav -vcodec copy -acodec copy " + path + "result.mov";
		
		try {
			shellCmd(command);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void shellCmd(String command) throws Exception {
		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(command);
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		process.waitFor();
		process.destroy();
	}
	
	
	
	
	
	
	public void mergeFiles(String ID) {
		
		File video = new File(FilePath.OUTPUTPATH + ID + "/resultTemp/video.mov");
		File audio = new File(FilePath.OUTPUTPATH + ID + "/resultTemp/audio.wav");
		
		try {
			DataSource videoDataSource = javax.media.Manager
					.createDataSource(video.toURI().toURL()); // your video file
			DataSource audioDataSource = javax.media.Manager
					.createDataSource(audio.toURI().toURL()); // your
																		// audio
																		// file
			DataSource mixedDataSource = null; // data source to combine video
												// with audio
			DataSource arrayDataSource[] = new DataSource[2]; // data source
																// array
			DataSource outputDataSource = null; // file to output

			DataSink outputDataSink = null; // datasink for output file
			
			/*
			MediaLocator videoLocator = new MediaLocator(video.toURI().toURL()); // media
																		// locator
																		// for
																		// video
			MediaLocator audioLocator = new MediaLocator(audio.toURI()
					.toURL()); // media locator for audio
			*/
			
			FileTypeDescriptor outputType = new FileTypeDescriptor(
					FileTypeDescriptor.QUICKTIME); // output video format type

			Format outputFormat[] = new Format[2]; // format array
			VideoFormat videoFormat = new VideoFormat(VideoFormat.JPEG); // output
																			// video
																			// codec
																			// MPEG
																			// does
																			// not
																			// work
																			// on
																			// windows
			javax.media.format.AudioFormat audioMediaFormat = new javax.media.format.AudioFormat(
					javax.media.format.AudioFormat.LINEAR, 44100, 16, 2); // audio
																			// format

			outputFormat[0] = videoFormat;
			outputFormat[1] = audioMediaFormat;

			// create processors for each file
			Processor videoProcessor = Manager.createProcessor(videoDataSource);
			Processor audioProcessor = Manager.createProcessor(audioDataSource);
			Processor processor = null;

			// start video and audio processors
			videoProcessor.realize();
			audioProcessor.realize();
			// wait till they are realized
			while (videoProcessor.getState() != 300
					&& audioProcessor.getState() != 300) {
				Thread.sleep(100);
			}
			// get processors dataoutputs to merge
			arrayDataSource[0] = videoProcessor.getDataOutput();
			arrayDataSource[1] = audioProcessor.getDataOutput();

			videoProcessor.start();
			audioProcessor.start();

			// create merging data source
			mixedDataSource = javax.media.Manager
					.createMergingDataSource(arrayDataSource);
			mixedDataSource.connect();
			mixedDataSource.start();
			// init final processor to create merged file
			ProcessorModel processorModel = new ProcessorModel(mixedDataSource,
					outputFormat, outputType);
			processor = Manager.createRealizedProcessor(processorModel);
			processor.addControllerListener(this);
			processor.configure();
			// wait till configured
			while (processor.getState() < 180) {
				Thread.sleep(20);
			}

			processor.setContentDescriptor(new ContentDescriptor(
					FileTypeDescriptor.QUICKTIME));

			TrackControl tcs[] = processor.getTrackControls();
			Format f[] = tcs[0].getSupportedFormats();

			tcs[0].setFormat(f[0]);

			processor.realize();
			// wait till realized
			while (processor.getState() < 300) {
				Thread.sleep(20);
			}
			// create merged file and start writing media to it
			outputDataSource = processor.getDataOutput();
			MediaLocator outputLocator = new MediaLocator("file:/" + FilePath.OUTPUTPATH + ID + "/resultTemp/result.mov");
			outputDataSink = Manager.createDataSink(outputDataSource,
					outputLocator);
			outputDataSink.open();
			outputDataSink.addDataSinkListener(this);
			outputDataSink.start();
			processor.start();

			while (processor.getState() < 500) {
				Thread.sleep(100);
			}
			// wait until writing is done
			waitForFileDone();
			// dispose processor and datasink
			outputDataSink.stop();
			processor.stop();

			outputDataSink.close();
			processor.close();
			

		} catch (NoDataSourceException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IncompatibleSourceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoDataSinkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoProcessorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CannotRealizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		File fromFile = new File(FilePath.OUTPUTPATH + ID + "/videoTemp/result.mov");//move to output movie
		File toFile = new File(FilePath.OUTPUTPATH + ID + "/download/result.mov");//transfer web server PC
		boolean result = fromFile.renameTo(toFile);
		System.out.println("이동 성공여부 : " + result);
		*/
	}

	Object waitFileSync = new Object();
	boolean fileDone = false;
	boolean fileSuccess = true;
	Object waitSync = new Object();
	boolean stateTransitionOK = true;

	/**
	 * Block until file writing is done.
	 */
	boolean waitForFileDone() {
		synchronized (waitFileSync) {
			try {
				while (!fileDone)
					waitFileSync.wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return fileSuccess;
	}

	/**
	 * Event handler for the file writer.
	 */
	public void dataSinkUpdate(DataSinkEvent evt) {

		if (evt instanceof EndOfStreamEvent) {
			synchronized (waitFileSync) {
				fileDone = true;
				waitFileSync.notifyAll();
			}
		} else if (evt instanceof DataSinkErrorEvent) {
			synchronized (waitFileSync) {
				fileDone = true;
				fileSuccess = false;
				waitFileSync.notifyAll();
			}
		}
	}

	@Override
	public void controllerUpdate(ControllerEvent evt) {
		if (evt instanceof ConfigureCompleteEvent
				|| evt instanceof RealizeCompleteEvent
				|| evt instanceof PrefetchCompleteEvent) {
			synchronized (waitSync) {
				stateTransitionOK = true;
				waitSync.notifyAll();
			}
		} else if (evt instanceof ResourceUnavailableEvent) {
			synchronized (waitSync) {
				stateTransitionOK = false;
				waitSync.notifyAll();
			}
		} else if (evt instanceof EndOfMediaEvent) {
			evt.getSourceController().stop();
			evt.getSourceController().close();
		}

	}
}