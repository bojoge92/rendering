package com.render.struct;

public class BasicInfo{
	
	private long startTime;
	private long endTime;
	private String fileName;
	
	
	public BasicInfo(String fileName, long startTime, long endTime){
		this.fileName = fileName;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getFileName(){
		return fileName;
	}
	public long getStartTime(){
		return startTime;
	}
	public long getEndTime(){
		return endTime;
	}
	
}