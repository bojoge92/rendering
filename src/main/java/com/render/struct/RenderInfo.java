package com.render.struct;

public class RenderInfo{

	private String renderInfo;
	private long startTime;
	private long endTime;
	private String text;
	private String resize;
	
	
	public RenderInfo(String renderInfo, long startTime, long endTime, String text, String resize){
		this.renderInfo = renderInfo;
		this.startTime = startTime;
		this.endTime = endTime;
		this.text = text;
		this.resize = resize;
	}

	public String getRenderInfo(){
		return renderInfo;
	}
	public long getStartTime(){
		return startTime;
	}
	public long getEndTime(){
		return endTime;
	}
	public String getText(){
		return text;
	}
	public String getResize(){
		return resize;
	}
	
}