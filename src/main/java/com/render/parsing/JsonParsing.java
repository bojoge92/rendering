package com.render.parsing;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.render.struct.BasicInfo;
import com.render.struct.RenderInfo;

public class JsonParsing {

	private String receive;
	
	public void setData(String receive) {
		this.receive = receive;
	}

	public String getFileName() {
		JSONObject jo;
		jo = (JSONObject) JSONValue.parse(receive);

		String fileName;
		fileName = (String) jo.get("FileName");

		return fileName;
	}
	
	public String getId() {
		JSONObject jo;
		jo = (JSONObject) JSONValue.parse(receive);

		String id;
		id = (String) jo.get("Id");

		return id;
	}

	public String getResultFileName() {
		JSONObject jo;
		jo = (JSONObject) JSONValue.parse(receive);

		String resultFileName;
		resultFileName = (String) jo.get("Name");

		return resultFileName;
	}
	

	public ArrayList<BasicInfo> getBasicList(){
		ArrayList<BasicInfo> list = new ArrayList<BasicInfo>();

		JSONObject jo;
		jo = (JSONObject) JSONValue.parse(receive);
				
		JSONArray ja = (JSONArray)jo.get("baseInfo");
		
		Iterator it = ja.iterator();
		
		while(it.hasNext()){
		   JSONObject joTemp = (JSONObject)it.next();
		   String fileName = (String) joTemp.get("F");
		   long startTime = (Long) joTemp.get("S");
		   long endTime = (Long) joTemp.get("E");
		   
		   list.add(new BasicInfo(fileName, startTime, endTime));
		}
				
		return list;		
	}
	
	
	public ArrayList<RenderInfo> getRenderList() {
		
		ArrayList<RenderInfo> list = new ArrayList<RenderInfo>();
		
		JSONObject jo;
		jo = (JSONObject) JSONValue.parse(receive);

		JSONArray ja = (JSONArray) jo.get("renderInfo");

		Iterator it = ja.iterator();

		while (it.hasNext()) {
			JSONObject joTemp = (JSONObject) it.next();
			String renderInfo = (String) joTemp.get("R");
			long startTime = (Long) joTemp.get("S");
			long endTime = (Long) joTemp.get("E");
			String text = (String) joTemp.get("Text");
			String resize = (String) joTemp.get("Resize");

			list.add(new RenderInfo(renderInfo, startTime, endTime, text, resize));
		}

		return list;
	}

}
