package com.render.ipportcheck;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class GetIP {

	public String getIP() {

		Enumeration<NetworkInterface> nienum;
		try {
			nienum = NetworkInterface.getNetworkInterfaces();

			while (nienum.hasMoreElements()) {
				NetworkInterface ni = nienum.nextElement();
				Enumeration<InetAddress> kk = ni.getInetAddresses();

				while (kk.hasMoreElements()) {
					InetAddress inetAddress = (InetAddress) kk.nextElement();

					if (inetAddress.getHostName().matches(".*master.*")
							|| inetAddress.getHostName().matches(".*slave.*"))
						return inetAddress.getHostAddress();

				}

			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
}
