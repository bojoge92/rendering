package com.render.ipportcheck;

import java.io.IOException;
import java.net.Socket;

public class PortCheck {
	
	public int YARN_AM_FILE_SEND_PORT;
	public int YARN_AM_FILE_RECEIVE_PORT;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		PortCheck pc = new PortCheck();
		
		pc.available();
		System.out.println(pc.YARN_AM_FILE_SEND_PORT + " " + pc.YARN_AM_FILE_RECEIVE_PORT);
		
	}
	
	public void available() {
		
		int n = 0;
		
		for(int i=10000; i<20000; i++){
		
		    try (Socket ignored = new Socket("localhost", i)) {
		    } catch (IOException ignored) {

		    	n++;
		    	
		    	if(n == 1)
		    		YARN_AM_FILE_SEND_PORT = i;
		    	else if(n == 2)
		    		YARN_AM_FILE_RECEIVE_PORT = i;
		    	else if(n == 3)
		    		break;
		    	
		    }
	    
		}
		
	}
	
}
