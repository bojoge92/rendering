package com.render.filereceiver;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.apache.commons.lang.StringUtils;

public class FileReceiver{
	
	Socket socket;
	DataInputStream dis;
	FileOutputStream fos;
	BufferedOutputStream bos;
	
	DataOutputStream dos;
	
	public FileReceiver(Socket socket) {
		this.socket = socket;
	}

	public boolean receiveStart(String id, String fileName) {
		try {
			
			Socket socketTemp = socket;
			
			dos = new DataOutputStream(socketTemp.getOutputStream());
			
			byte[] out = new byte[4];
			
			
			out = intToByteArray(100);
			dos.write(out);
			dos.flush(); //이것은 꼭 해주자
			System.out.printf("접속 정보 전달.\n");
			
			
			
			
			
			System.out.println("파일 수신 작업을 시작합니다.");
			dis = new DataInputStream(socket.getInputStream());
			

			System.out.println("1111");
			
			//파일길이 받음
			byte[] in = new byte[10];
			dis.read(in, 0, in.length);
			//int fileSize = byteArrayToInt(in);
			
			String receive = new String(in, 0, in.length); //byte형을 string형으로 바꿔 초기화 한다.
			receive.trim();
			
			
			System.out.println("2222 : " + receive);
			
			
			int fileSize = stringToInt(receive);
			
			
			System.out.println("파일 크기 : " + fileSize);
			
			
			
			
			
			// 파일을 생성하고 파일에 대한 출력 스트림 생성
			File f = new File(FilePath.OUTPUTPATH + id + "/videoTemp/" + fileName);
			fos = new FileOutputStream(f);
			bos = new BufferedOutputStream(fos);	
			//System.out.println(fileName + " 파일을 생성하였습니다.");
			
			// 바이트 데이터를 전송받으면서 기록
			int size = 4096;
			byte[] buffer = new byte[size];
			
			int readSize = 0;
			
			while(readSize < fileSize){
				int rsize = dis.read(buffer);
				fos.write(buffer, 0, rsize);
				readSize += (long)rsize;
			}
			
			
			/*
			while (fileSize > temp) {
				len = dis.read(data);
				bos.write(data, 0, len);
			}
			*/
			
			
			dos.close();
			
			bos.flush();
			bos.close();
			fos.close();
			dis.close();
			System.out.println("파일 수신 작업을 완료하였습니다.");
			
			return true;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	

	public int stringToInt(String input){
		
		int result = 0;
		int i = 0;
		char[] c = input.toCharArray();
		
		while(true){
			
			if(i == c.length || c[i] < 48 || c[i] > 57)
				break;
			
			else{
				result *= 10;
				result += c[i]-48;
				i++;
			}
			
		}
		
		return result;
		
	}
	
	
	
	public int byteToint(byte[] arr){
	    return (arr[0] & 0xff)<<24 | (arr[1] & 0xff)<<16 |
	              (arr[2] & 0xff)<<8 | (arr[3] & 0xff);
	   }

	
	
	public byte[] longToBytes(long x) {
	    ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE);
	    buffer.putLong(x);
	    return buffer.array();
	}

	public long bytesToLong(byte[] bytes) {
	    ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE);
	    buffer.put(bytes);
	    buffer.flip();//need flip 
	    return buffer.getLong();
	}
	
	
	
	
	private static byte[] intToByteArray(final int integer) {
		ByteBuffer buff = ByteBuffer.allocate(Integer.SIZE / 8);
		buff.putInt(integer);
		buff.order(ByteOrder.BIG_ENDIAN);
		//buff.order(ByteOrder.LITTLE_ENDIAN);
		return buff.array();
	}
	
	private static int byteArrayToInt(byte[] bytes) {
		final int size = Integer.SIZE / 8;
		ByteBuffer buff = ByteBuffer.allocate(size);
		final byte[] newBytes = new byte[size];
		for (int i = 0; i < size; i++) {
			if (i + bytes.length < size) {
				newBytes[i] = (byte) 0x00;
			} else {
				newBytes[i] = bytes[i + bytes.length - size];
			}
		}
		buff = ByteBuffer.wrap(newBytes);
		buff.order(ByteOrder.BIG_ENDIAN); // Endian에 맞게 세팅
		return buff.getInt();
	}
	

	
	
	
	
}


