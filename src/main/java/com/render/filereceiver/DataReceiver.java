package com.render.filereceiver;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import com.render.folderdelete.FolderClean;
import com.render.parsing.JsonParsing;
import com.render.struct.BasicInfo;
import com.render.struct.RenderInfo;

public class DataReceiver {

	private ArrayList<BasicInfo> basicList = new ArrayList<BasicInfo>();
	private ArrayList<RenderInfo> renderList = new ArrayList<RenderInfo>();
	private String id;
	private String receiveFileName;
	private String resultFileName;
	private ArrayList<String> fileName = new ArrayList<String>();
	
	Socket socket;
	DataInputStream dis;
	
	
	public DataReceiver(Socket socket) {
		this.socket = socket;
	}
	
	public void receiveStart() {
		try {
			
			Socket socketTemp = socket;
			
			System.out.println("데이터 수신 작업을 시작합니다.");
			dis = new DataInputStream(socketTemp.getInputStream());
			
			
			// 파일명을 전송 받고 파일명 수정.
			//String receiveFileName = dis.readUTF();
			byte[] in = new byte[2048];
			dis.read(in, 0, in.length);
			String receive = new String(in, 0, in.length); //byte형을 string형으로 바꿔 초기화 한다.
			//System.out.println("1 " + receive);
			
			receive = receive.trim();   //trim()을 해주지 않으면 공백부분이 제대로 출력되지 않는다.
			System.out.println("2 " + receive);
			
			
			/*
			dos = new DataOutputStream(socketTemp.getOutputStream());			
			byte[] out = new byte[1024];
			String s = "이거보내";
			out = s.getBytes();  //string 형을 byte형으로 바꿔 대입한다.
			dos.write(out);
			dos.flush(); //이것은 꼭 해주자
			*/
			
			
			
			
			// 받은 데이터 파싱
			JsonParsing jp = new JsonParsing();
			jp.setData(receive);
			
			id = jp.getId();
			receiveFileName = jp.getFileName();
			basicList = jp.getBasicList();
			renderList = jp.getRenderList();
			resultFileName = jp.getResultFileName();

			String[] temp = receiveFileName.split("&&");
			for (int i = 0; i < temp.length; i++)
				fileName.add(temp[i]);

			dis.close();
			System.out.println("데이터 수신 작업을 완료하였습니다.");
			
			FolderClean.clean(id);// 해당 아이디에 맞는 경로 생성
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getID() {
		return id;
	}

	public ArrayList<BasicInfo> getBasicInfo() {
		return basicList;
	}

	public ArrayList<RenderInfo> getRenderInfo() {
		return renderList;
	}

	public ArrayList<String> getFileName() {
		return fileName;
	}

	public String getResultFileName() {
		return resultFileName;
	}

}
