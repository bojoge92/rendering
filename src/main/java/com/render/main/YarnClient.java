/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.render.main;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

import com.render.filereceiver.FilePath;
import com.render.jni.BW;
import com.render.jni.Blur;
import com.render.jni.Mosaic;
import com.render.jni.Resize;
import com.render.jni.Text;

public class YarnClient {
	private static final long MEGABYTE = 1024L * 1024L;
	
	//final static int YARN_CLIENT_FILE_RECEIVE_PORT = 7777;
	//final static int YARN_CLIENT_FILE_SEND_PORT = 7788;
	static int YARN_CLIENT_FILE_RECEIVE_PORT;
	static int YARN_CLIENT_FILE_SEND_PORT;
	
	// 클라이언트로 부터 자료 수신
	String applicationMasterIP = null;
	Socket receiveSocket = null;
	Socket sendSocket = null;
	
	DataInputStream dis = null;
	
	//파일 받아오기용
	FileOutputStream fos = null;
	BufferedOutputStream bos = null;
	
	//파일 보내기용
	FileInputStream fis = null;
	BufferedInputStream bis = null;
	DataOutputStream dos = null;
	
	

	public YarnClient(String clientIP, String applicationMasterIP) {
		System.out.println("HelloYarn!");
		System.out.println("Client IP : " + clientIP);
		System.out.println("Application Master IP : " + applicationMasterIP);
	}

	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	public void printMemoryStats() {
		long freeMemory = bytesToMegabytes(Runtime.getRuntime().freeMemory());
		long totalMemory = bytesToMegabytes(Runtime.getRuntime().totalMemory());
		long maxMemory = bytesToMegabytes(Runtime.getRuntime().maxMemory());

		System.out.println("The amount of free memory in the Java Virtual Machine: " + freeMemory);
		System.out.println("The total amount of memory in the Java virtual machine: " + totalMemory);
		System.out.println("The maximum amount of memory that the Java virtual machine: " + maxMemory);
	}
	
	public void receiveFile(String clientIP, String amIP) {

		applicationMasterIP = amIP;
		
		while(true){
			
			try {
				// 서버 연결
				receiveSocket = new Socket(applicationMasterIP, YARN_CLIENT_FILE_RECEIVE_PORT);
				//System.out.println("서버에 연결되었습니다.");
				
				dis = new DataInputStream(receiveSocket.getInputStream());
										
				String fileName = dis.readUTF();//파일명 전송 받음
				
				//System.out.println("FileName : " + fileName);
				
				if(fileName.equals("EOF"))//종료일때
					break;
				
				
				/** 종료 아닌 실제 렌더링 작업 */
				String renderInfo = dis.readUTF();//렌더링 정보 전송 받음
				String textInfo = dis.readUTF();//텍스트 정보 전송 받음
				String textSizeInfo = dis.readUTF();//텍스트 위치, 크기 정보 전송 받음
				String resizeInfo = dis.readUTF();//해상도 정보 전송 받음
				//System.out.println(renderInfo);
				String[] renderArr = renderInfo.split("&&");
				String[] resizeArr = resizeInfo.split("\\*");//가로와 세로 나눔
				
				//System.out.println(renderInfo + " " + textInfo + " " + textSizeInfo + " " + resizeInfo);
				
				//실제 렌더링 작업할 yarn 클라이언트용 렌더링 폴더 없으면 생성
				File f = new File(FilePath.OUTPUTPATH + "RenderImageTemp");
				if(!f.exists())
					f.mkdirs();
				
				// 파일을 생성하고 파일에 대한 출력 스트림 생성
				f = new File(FilePath.OUTPUTPATH + "RenderImageTemp/" + clientIP + fileName);
				fos = new FileOutputStream(f);
				bos = new BufferedOutputStream(fos);
				
				// 바이트 데이터를 전송받으면서 기록
				int len;
				int size = 4096;
				byte[] data = new byte[size];
				
				while ((len = dis.read(data)) != -1) {
					bos.write(data, 0, len);
					bos.flush();
				}
				
				
				dis.close();
				bos.close();
				fos.close();
				
				
				//System.out.println("전송 끝 작업시작");
				//for(int i=0; i<renderArr.length; i++){					
				//	System.out.print(renderArr[i] + " ");
				//}
				//System.out.println();
				
				/** 여기서 전송받은 이미지 (f파일)에 대해 렌더링 작업 수행 */
				//자막 = text
				//블러 = blur
				//모자이크 = moza
				//흑백 = bw
				//해상도 = resize
				
				for(int i=0; i<renderArr.length; i++){
					//System.out.print(renderArr[i] + " ");
					switch(renderArr[i]){
					case "T":
						String[] temp = textSizeInfo.split("\\*");
						Text t = new Text();
						t.text(f.getAbsolutePath(), textInfo, Integer.parseInt(temp[0]), Integer.parseInt(temp[1]), Integer.parseInt(temp[2]));
						//System.out.println(f.getAbsolutePath() + " " + textInfo + " " + Integer.parseInt(temp[0]) + " " +	Integer.parseInt(temp[1]) + " " + Integer.parseInt(temp[2]));
						break;
						
					case "B":
						Blur b = new Blur();
						b.blur(f.getAbsolutePath());						
						break;
						
					case "M":
						Mosaic m = new Mosaic();
						m.mosaic(f.getAbsolutePath());
						break;
						
					case "G":
						BW bw = new BW();
						bw.bw(f.getAbsolutePath());
						break;
						
					case "resize":						
						Resize r = new Resize();
						r.resize(f.getAbsolutePath(), Integer.parseInt(resizeArr[0]), Integer.parseInt(resizeArr[1]));
						
						break;	
					}
					//System.out.println(" 끝");
				}
				
			    
			    System.out.println("작업끝");
				
				/** 렌더링 완료된 이미지 서버로 전송 */
				
				// 서버 연결
				sendSocket = new Socket(applicationMasterIP, YARN_CLIENT_FILE_SEND_PORT);
				//System.out.println("서버에 연결되었습니다.");
				
				dos = new DataOutputStream(sendSocket.getOutputStream());
				
				
				// 렌더링 완료한 이미지파일 어플리케이션 마스터로 전송
				
				dos.writeUTF(fileName);//파일명 전송
				
				// 파일 내용을 읽으면서 전송
				f = new File(FilePath.OUTPUTPATH + "RenderImageTemp/" + clientIP + fileName);
						
				fis = new FileInputStream(f);
				bis = new BufferedInputStream(fis);
							
				size = 4096;
				data = new byte[size];
				
				
				while ((len = bis.read(data)) != -1) {
					dos.write(data, 0, len);
				}
				
				
				bis.close();
				fis.close();
				dos.close();
				
				//System.out.println("yarn client word end!!");
				
				//작업 완료한 이미지파일 삭제
				f.delete();
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		}
		
	}
	

	public static void main(String[] args) {
		String clientIP = args[1];
		String applicationMasterIP = args[2];
		
		YARN_CLIENT_FILE_RECEIVE_PORT = Integer.parseInt(args[3]);
		YARN_CLIENT_FILE_SEND_PORT = Integer.parseInt(args[4]);
		
		//System.out.println("이거이거 : " + YARN_CLIENT_FILE_RECEIVE_PORT + " " + YARN_CLIENT_FILE_SEND_PORT);
		
		YarnClient helloYarn = new YarnClient(args[1], args[2]);
		// helloYarn.printMemoryStats();
		helloYarn.receiveFile(clientIP, applicationMasterIP);
	}
}
