/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.render.main;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.ExitUtil;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.api.protocolrecords.AllocateResponse;
import org.apache.hadoop.yarn.api.records.*;
import org.apache.hadoop.yarn.client.api.AMRMClient;
import org.apache.hadoop.yarn.client.api.AMRMClient.ContainerRequest;
import org.apache.hadoop.yarn.client.api.NMClient;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.util.ConverterUtils;
import org.apache.hadoop.yarn.util.Records;
import org.apache.log4j.LogManager;

import com.render.basicrender.AudioConcat;
import com.render.basicrender.ExtractAudioFromVideo;
import com.render.basicrender.MakeVideoUseImage;
import com.render.basicrender.MergeAudioVideo;
import com.render.basicrender.SplitFrameFromVideo;
import com.render.basicrender.VideoMergeSort;
import com.render.filereceiver.DataReceiver;
import com.render.filereceiver.FilePath;
import com.render.filereceiver.FileReceiver;
import com.render.folderdelete.FolderClean;
import com.render.ipportcheck.GetIP;
import com.render.ipportcheck.PortCheck;
import com.render.struct.BasicInfo;
import com.render.struct.RenderInfo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class MyApplicationMaster {

	int fileCount = 0;
	
	final static int PORT = 4444;// slave file receive PORT
	final static String WEB_SERVER_IP = "112.108.40.179";// file download web
															// server ip / port
	final static int WEB_SERVER_PORT = 5555;
	//final static int YARN_AM_FILE_SEND_PORT = 7777;
	//final static int YARN_AM_FILE_RECEIVE_PORT = 7788;
	static int YARN_AM_FILE_SEND_PORT;
	static int YARN_AM_FILE_RECEIVE_PORT;

	private static final Log LOG = LogFactory.getLog(MyApplicationMaster.class);

	// Application Attempt Id ( combination of attemptId and fail count )
	protected ApplicationAttemptId appAttemptID;

	// Client IP
	private String clientIP = "";

	// No. of containers to run shell command on
	private int numTotalContainers = 1;

	// Memory to request for the container on which the shell command will run
	private int containerMemory = 10;

	// VirtualCores to request for the container on which the shell command will
	// run
	private int containerVirtualCores = 1;

	// Priority of the request
	private int requestPriority;

	// Location of shell script ( obtained from info set in env )
	// Shell script path in fs
	private String appJarPath = "";
	// Timestamp needed for creating a local resource
	private long appJarTimestamp = 0;
	// File length needed for local resource
	private long appJarPathLen = 0;

	// Configuration
	private Configuration conf;

	public String id = "";// 전송받을 클라이언트 사용자 아이디
	public ArrayList<BasicInfo> basicList = new ArrayList<BasicInfo>();// 전송받을
																			// 비디오
																			// 기본
																			// 순서
																			// 정보
	public ArrayList<RenderInfo> renderList = new ArrayList<RenderInfo>();// 전송받을
																			// 비디오
																			// 렌더링
																			// 정보
	public String resultFileName = "";// 다운로드 서버에 전송할 결과 파일 이름
	public ArrayList<String> fileName = new ArrayList<String>();// 전송받을 파일 이름
	
	ServerSocket sendSocket;
	ServerSocket receiveSocket;

	public MyApplicationMaster() {
		// Set up the configuration
		conf = new YarnConfiguration();
		
	}
	
	/**
	 * Parse command line options
	 *
	 * @param args
	 *            Command line args
	 * @return Whether init successful and run should be invoked
	 * @throws org.apache.commons.cli.ParseException
	 * @throws java.io.IOException
	 */
	public boolean init(String[] args) throws Exception {
		Options opts = new Options();
		opts.addOption("app_attempt_id", true, "App Attempt ID. Not to be used unless for testing purposes");
		opts.addOption("shell_env", true, "Environment for shell script. Specified as env_key=env_val pairs");
		opts.addOption("client_IP", true, "Client IP");
		opts.addOption("container_memory", true, "Amount of memory in MB to be requested to run the shell command");
		opts.addOption("container_vcores", true, "Amount of virtual cores to be requested to run the shell command");
		opts.addOption("num_containers", true, "No. of containers on which the shell command needs to be executed");
		opts.addOption("priority", true, "Application Priority. Default 0");
		opts.addOption("help", false, "Print usage");

		CommandLine cliParser = new GnuParser().parse(opts, args);

		Map<String, String> envs = System.getenv();

		if (!envs.containsKey(ApplicationConstants.Environment.CONTAINER_ID.name())) {
			if (cliParser.hasOption("app_attempt_id")) {
				String appIdStr = cliParser.getOptionValue("app_attempt_id", "");
				appAttemptID = ConverterUtils.toApplicationAttemptId(appIdStr);
			} else {
				throw new IllegalArgumentException("Application Attempt Id not set in the environment");
			}
		} else {
			ContainerId containerId = ConverterUtils
					.toContainerId(envs.get(ApplicationConstants.Environment.CONTAINER_ID.name()));
			appAttemptID = containerId.getApplicationAttemptId();
		}

		if (!envs.containsKey(ApplicationConstants.APP_SUBMIT_TIME_ENV)) {
			throw new RuntimeException(ApplicationConstants.APP_SUBMIT_TIME_ENV + " not set in the environment");
		}
		if (!envs.containsKey(ApplicationConstants.Environment.NM_HOST.name())) {
			throw new RuntimeException(ApplicationConstants.Environment.NM_HOST.name() + " not set in the environment");
		}
		if (!envs.containsKey(ApplicationConstants.Environment.NM_HTTP_PORT.name())) {
			throw new RuntimeException(ApplicationConstants.Environment.NM_HTTP_PORT + " not set in the environment");
		}
		if (!envs.containsKey(ApplicationConstants.Environment.NM_PORT.name())) {
			throw new RuntimeException(ApplicationConstants.Environment.NM_PORT.name() + " not set in the environment");
		}

		if (envs.containsKey(Constants.AM_JAR_PATH)) {
			appJarPath = envs.get(Constants.AM_JAR_PATH);

			if (envs.containsKey(Constants.AM_JAR_TIMESTAMP)) {
				appJarTimestamp = Long.valueOf(envs.get(Constants.AM_JAR_TIMESTAMP));
			}
			if (envs.containsKey(Constants.AM_JAR_LENGTH)) {
				appJarPathLen = Long.valueOf(envs.get(Constants.AM_JAR_LENGTH));
			}

			if (!appJarPath.isEmpty() && (appJarTimestamp <= 0 || appJarPathLen <= 0)) {
				LOG.error("Illegal values in env for shell script path" + ", path=" + appJarPath + ", len="
						+ appJarPathLen + ", timestamp=" + appJarTimestamp);
				throw new IllegalArgumentException("Illegal values in env for shell script path");
			}
		}

		LOG.info("Application master for app" + ", appId=" + appAttemptID.getApplicationId().getId()
				+ ", clusterTimestamp=" + appAttemptID.getApplicationId().getClusterTimestamp() + ", attemptId="
				+ appAttemptID.getAttemptId());

		clientIP = cliParser.getOptionValue("client_IP", "");
		containerMemory = Integer.parseInt(cliParser.getOptionValue("container_memory", "10"));
		containerVirtualCores = Integer.parseInt(cliParser.getOptionValue("container_vcores", "1"));
		numTotalContainers = Integer.parseInt(cliParser.getOptionValue("num_containers", "1"));
		if (numTotalContainers == 0) {
			throw new IllegalArgumentException("Cannot run MyAppliCatioaster with no containers");
		}

		LOG.info("This is Priority Test : " + Integer.parseInt(cliParser.getOptionValue("priority", "0")));
		requestPriority = Integer.parseInt(cliParser.getOptionValue("priority", "0"));

		return true;
	}

	/**
	 * Main run function for the application master
	 *
	 * @throws org.apache.hadoop.yarn.exceptions.YarnException
	 * @throws java.io.IOException
	 */
	@SuppressWarnings({ "unchecked" })
	public void run() throws Exception {
		LOG.info("Running MyApplicationMaster");

		long startTime = System.currentTimeMillis();

		int i;
		
		fileReceiver();
		
		int wantFrame = 25;
		
		
		/*
		System.out.println(id);
		
		for(i=0; i<renderList.size(); i++){
			System.out.println(renderList.get(i).getStartTime() + " " + renderList.get(i).getEndTime()
					 + " " + renderList.get(i).getResize() + " " + renderList.get(i).getRenderInfo()
					 + " " + renderList.get(i).getText());
			System.out.println(basicList.get(i).getStartTime() + " " + basicList.get(i).getEndTime()
					 + " " + basicList.get(i).getFileName());
		}
		*/
		
		
		//double frameTemp = 1 / (double) wantFrame;// want frame
		// double frameTemp = 1 / (double)coder.getFrameRate().getDouble();//
		// want frame
		//long frame = (long) (Global.DEFAULT_PTS_PER_SECOND * frameTemp);
		
		
		
		// 영상 원하는 부분만 자르기
		/*
		for (i = 0; i < basicList.size(); i++) {
			File temp = new File(FilePath.OUTPUTPATH + id + "/videoTemp/" + basicList.get(i).getFileName());
			
			while (true) {
				if (temp.exists()) {
					VideoCut vc = new VideoCut();
					vc.VideoCut(basicList.get(i).getFileName(), id, basicList.get(i).getStartTime(),
							basicList.get(i).getEndTime() - basicList.get(i).getStartTime(), i);
					break;
				}
			}
		}
		System.out.println("영상 자르기 work end!!");
		*/
		
		
		// 음성분리
		for (i = 0; i < basicList.size(); i++) {
			String fileFormat = basicList.get(i).getFileName().substring(
					basicList.get(i).getFileName().lastIndexOf('.'), basicList.get(i).getFileName().length());
			String temp = i + fileFormat;
			
			
			
			ExtractAudioFromVideo eav = new ExtractAudioFromVideo();
			eav.soundExtract(basicList.get(i).getFileName(),
					id,
					i,
					basicList.get(i).getStartTime(), 
					basicList.get(i).getEndTime() - basicList.get(i).getStartTime());
			
		}
		System.out.println("음성 분리 work end!!");
		
		
		
		AudioConcat.concatAudio(i, id);
		System.out.println("음성 연결 work end!!");
		
		

		SplitFrameFromVideo sffv;
		// long numbering = 0;
		for (i = 0; i < basicList.size(); i++) {
			String fileFormat = basicList.get(i).getFileName().substring(
					basicList.get(i).getFileName().lastIndexOf('.'), basicList.get(i).getFileName().length());
			
			System.out.println(basicList.get(i).getFileName() + " " + id + " " + Integer.toString(i) + " " + wantFrame + " " + 
					basicList.get(i).getStartTime() + " " + 
					(basicList.get(i).getEndTime() - basicList.get(i).getStartTime()));
			
			sffv = new SplitFrameFromVideo(basicList.get(i).getFileName(),
					id, 
					Integer.toString(i),
					wantFrame,
					basicList.get(i).getStartTime(),
					basicList.get(i).getEndTime() - basicList.get(i).getStartTime());
			sffv.run();// 동영상 프레임 쪼개기
			
			
		}
		
		System.out.println("프레임 나누기 work end!!");
		
		
		
		VideoMergeSort.mergeSort(id, basicList.size());
		System.out.println("프레임 모으기 work end!!");
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Initialize clients to ResourceManager and NodeManagers
		AMRMClient<ContainerRequest> amRMClient = AMRMClient.createAMRMClient();
		amRMClient.init(conf);
		amRMClient.start();

		// Register with ResourceManager
		amRMClient.registerApplicationMaster("", 0, "");
		
		// Set up resource type requirements for Container
		Resource capability = Records.newRecord(Resource.class);
		capability.setMemory(containerMemory);
		capability.setVirtualCores(containerVirtualCores);
				
		// Priority for worker containers - priorities are intra-application
		Priority priority = Records.newRecord(Priority.class);
		priority.setPriority(requestPriority);
		
		// Make container requests to ResourceManager
		for (i = 0; i < numTotalContainers; ++i) {
			ContainerRequest containerAsk = new ContainerRequest(capability, null, null, priority);
			amRMClient.addContainerRequest(containerAsk);
		}

		NMClient nmClient = NMClient.createNMClient();
		nmClient.init(conf);
		nmClient.start();
		
		// Setup CLASSPATH for Container
		Map<String, String> containerEnv = new HashMap<String, String>();
		containerEnv.put("CLASSPATH", "./*");

		// Setup ApplicationMaster jar file for Container
		LocalResource appMasterJar = createAppMasterJar();
		
		// Obtain allocated containers and launch
		int allocatedContainers = 0;
		// We need to start counting completed containers while still allocating
		// them since initial ones may complete while we're allocating
		// subsequent
		// containers and if we miss those notifications, we'll never see them
		// again
		// and this ApplicationMaster will hang indefinitely.
		int completedContainers = 0;
		while (allocatedContainers < numTotalContainers) {
			AllocateResponse response = amRMClient.allocate(0);
			
			for (Container container : response.getAllocatedContainers()) {
				allocatedContainers++;
				
				ContainerLaunchContext appContainer = createContainerLaunchContext(appMasterJar, containerEnv);
				LOG.info("Launching container " + allocatedContainers);
				
				nmClient.startContainer(container, appContainer);
			}
			
			for (ContainerStatus status : response.getCompletedContainersStatuses()) {
				++completedContainers;
				LOG.info("ContainerID:" + status.getContainerId() + ", state:" + status.getState().name());
			}
			Thread.sleep(100);
		}
		
		
		
		//렌더링 정보 저장 배열. 갯수만큼
		File folder = new File(FilePath.OUTPUTPATH + id + "/imageTemp/All");// image frame path
		File[] listOfFiles = folder.listFiles();
		
		String[] renderArr = new String[(listOfFiles.length)/wantFrame];//어떤 렌더링인지
		String[] textArr = new String[(listOfFiles.length)/wantFrame];//자막 있다면 무엇인지
		String[] textSizeArr = new String[(listOfFiles.length)/wantFrame];//자막 위치, 크기는 무엇인지
		String[] resizeArr = new String[(listOfFiles.length)/wantFrame];//해상도는 얼마인지
		
		
		
		/** 리사이징 등의 슬레이브 프레임 영상작업 여기서 */
		
		for(i=0; i<renderList.size(); i++){
			String renderInfo = renderList.get(i).getRenderInfo();
			int start = (int) renderList.get(i).getStartTime();
			int end = (int) renderList.get(i).getEndTime();
			String text = renderList.get(i).getText();
			String resize = renderList.get(i).getResize();
			

			if(renderInfo.equals("T")){
				
				for(int k=0; k<renderArr.length; k++){
					textArr[k] = text;
					textSizeArr[k] = start + "*" + end + "*" + resize;
					
					if(renderArr[k] == null)
						renderArr[k] = "T";
					else
						renderArr[k] += "&&T";
					
				}
				
			}
			
			else{
				
				for(int j=start; j<end; j++){
					if(renderArr[j] == null)
						renderArr[j] = renderInfo;
					else
						renderArr[j] += "&&" + renderInfo;
					
					
					if(renderInfo.equals("resize"))
						resizeArr[j] = resize;
						
				}
				
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		ServerSender sender;
		ServerReceiver receiver;
		
		// 리스너 소켓 생성
		sendSocket = new ServerSocket(YARN_AM_FILE_SEND_PORT);
		receiveSocket = new ServerSocket(YARN_AM_FILE_RECEIVE_PORT);
		
		int percent = -1;
		
		try {
			Socket socket;
			
			for (i = 0; i < renderArr.length; i++) {
				
				if(renderArr[i] != null){
					
					for(int j = 0; j<wantFrame; j++){
						//System.out.println(renderArr[i] + " " + textArr[i] + " " + resizeArr[i]);
						//System.out.println((i*wantFrame) + j);
						socket = sendSocket.accept();
						sender = new ServerSender(socket, (i*wantFrame) + j, renderArr[i], textArr[i], textSizeArr[i], resizeArr[i]);
						sender.start();
						
						receiver = new ServerReceiver();//송신 하나당 수신 하나 실행
						receiver.start();	
						
						//Thread.sleep(50);

						//System.out.println( (int) ((((i*wantFrame) + j) / (double)(renderArr.length*wantFrame))*100) );
						if( percent != (int) ((((i*wantFrame) + j) / (double)(renderArr.length*wantFrame))*100) ){
							percent = (int) ((((i*wantFrame) + j) / (double)(renderArr.length*wantFrame))*100);
							System.out.println("Rendering percentage : " + percent);
						}
						
					}
					
				}
				
			}

		} catch (IOException e) {
			System.out.println("Yarn Client Data Tranfer Error");
			e.printStackTrace();
		}
		
		
		//종료 명령어 송신
		Socket socket;
		DataOutputStream dos;
		for(i=0; i<numTotalContainers; i++){
			socket = sendSocket.accept();
			
			dos = new DataOutputStream(socket.getOutputStream());
			
			dos.writeUTF("EOF");
		}
		
		
		//스레드 종료 대기
		/*
		System.out.println("스레드 종료 대기");		
		for(i = 0; i<numTotalContainers; i++){
			sender.join();
		}
		System.out.println("스레드 종료 완료");
		*/
		
		
		
		
		
		
		
		
		
		
		// Now wait for the remaining containers to complete
		while (completedContainers < numTotalContainers)
		{
			AllocateResponse response = amRMClient.allocate(completedContainers / numTotalContainers);
			for (ContainerStatus status : response.getCompletedContainersStatuses()) {
				++completedContainers;
				LOG.info("ContainerID:" + status.getContainerId() + ", state:" + status.getState().name());
			}
			
			/*
			System.out.println(id);
			for(i=0; i<renderList.size(); i++){
				System.out.println(renderList.get(i).getStartTime() + " " + renderList.get(i).getEndTime()
						 + " " + renderList.get(i).getRenderInfo());
			}
			*/
			
			Thread.sleep(100);
		}

		LOG.info("Completed containers:" + completedContainers);

		
		
		
		
		
		
		
		
		
		
		
		/** 렌더링 완료후 영상 제작 */
		
		String resize = renderList.get(0).getResize();
		String[] resizeWH = resize.split("\\*");
		
		System.out.println("이거이거 " + resizeWH[0] + " " + resizeWH[1]);
		
		MakeVideoUseImage.createVideo(id, 
				basicList.size(),
				Integer.parseInt(resizeWH[0]), 
				Integer.parseInt(resizeWH[1]), 
				wantFrame);// 쪼개진 프레임으로 해상도에 맞는 영상 제작
		
		System.out.println("영상제작 work end!!");

		/** 파일 생성 딜레이 커버 */

		File fileTemp = new File(FilePath.OUTPUTPATH + id + "/resultTemp/video.mov");
		while (true) {
			if (fileTemp.exists()) {
				break;
			}
		}
		
		
		MergeAudioVideo merge = new MergeAudioVideo();
		merge.mergeFiles2(id);
		
		System.out.println("영상음성합치기 work end!!");
		
		fileTransfer();
		//FolderClean.clean(id);
		
		
		System.out.println("All work end!!");
		
		
		/*
		File fileTemp1 = new File(FilePath.OUTPUTPATH + id + "/resultTemp/audio.wav");
		File fileTemp2 = new File(FilePath.OUTPUTPATH + id + "/resultTemp/video.mov");
		File fileTemp3 = new File(FilePath.OUTPUTPATH + id + "/resultTemp/result.mov");
		
		while (true) {
			if (fileTemp3.length() == (fileTemp1.length() + fileTemp2.length())) {
				break;
			}
		}
		*/

		
		
		// Un-register with ResourceManager
		amRMClient.unregisterApplicationMaster(FinalApplicationStatus.SUCCEEDED, "", "");
		LOG.info("Finished MyApplicationMaster");
		

		long endTime = System.currentTimeMillis();

		System.out.println( "실행 시간 : " + ( endTime - startTime )/1000.0 ); 
		
		
	}

	
	
	
	
	class ServerSender extends Thread {
		
		Socket socket;
		int fileNum;
		
		
		String renderInfo;
		String textInfo;
		String textSizeInfo;
		String resolInfo;
		
		DataOutputStream dos;
		
		//파일 보내기용
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		
		boolean flag = true;//종료 플래그
		
		public ServerSender(Socket socket, int fileNum, String renderInfo, String textInfo, String textSizeInfo, String resolInfo) {
			this.socket = socket;
			this.fileNum = fileNum;
			this.renderInfo = renderInfo;
			this.textInfo = textInfo;
			this.textSizeInfo = textSizeInfo;
			this.resolInfo = resolInfo;
		}
		
		@Override
		public void run() {
			
			try {
				
				dos = new DataOutputStream(socket.getOutputStream());
				
				//System.out.println("yarn 파일 송신이 시작되었습니다. " + fileNum);
				
				String sendFileName = String.format("%010d", fileNum) + ".jpg";
				//System.out.println("yarn 파일 송신이 시작되었습니다. " + sendFileName);
				
				dos.writeUTF(sendFileName);//파일이름 전송
				
				Thread.sleep(50);
				dos.writeUTF(renderInfo);//렌더링 정보 전송

				Thread.sleep(50);
				if(textInfo == null)
					textInfo = "";
				dos.writeUTF(textInfo);//텍스트 정보 전송

				Thread.sleep(50);
				if(textSizeInfo == null)
					textSizeInfo = "";
				dos.writeUTF(textSizeInfo);//텍스트 위치, 크기 정보 전송

				Thread.sleep(50);
				if(resolInfo == null)
					resolInfo = "";
				dos.writeUTF(resolInfo);//해상도 정보 전송
				

				Thread.sleep(50);
				// 파일 내용을 읽으면서 전송
				File f = new File(FilePath.OUTPUTPATH + id + "/imageTemp/All/" + sendFileName);
				fis = new FileInputStream(f);
				bis = new BufferedInputStream(fis);
				
				int len;
				int size = 4096;
				byte[] data = new byte[size];
				while ((len = bis.read(data)) != -1) {
					dos.write(data, 0, len);
					dos.flush();
				}
				
				bis.close();
				fis.close();
				dos.close();
				
				//LOG.info("Yarn Client로 파일전송 완료");
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}		
		
	}
	
	

	
	
	class ServerReceiver extends Thread{

		Socket socket;
		
		DataInputStream dis;

		//파일 받아오기용
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		
		public void run(){

			/** 파일 받을 준비 */
			try{
				
				//System.out.println("yarn 파일 수신 서버가 시작되었습니다.");
	
				socket = receiveSocket.accept();
				//System.out.println("클라이언트와 연결되었습니다.");
				
				dis = new DataInputStream(socket.getInputStream());
				
				String fileName = dis.readUTF();
				
				// 파일을 생성하고 파일에 대한 출력 스트림 생성
				dis = new DataInputStream(socket.getInputStream());
				
				File f = new File(FilePath.OUTPUTPATH + id + "/imageTemp/All/" + fileName);
				
				if(f.exists())//렌더링 전 파일 제거
					f.delete();
				
				fos = new FileOutputStream(f);
				bos = new BufferedOutputStream(fos);	
				
				// 바이트 데이터를 전송받으면서 기록
				
				int len;
				int size = 4096;
				byte[] data = new byte[size];
				
				while ((len = dis.read(data)) != -1) {
					bos.write(data, 0, len);
				}
				
				bos.flush();
				bos.close();
				fos.close();
				
				dis.close();

				//System.out.println("yarn 파일 수신이 완료되었습니다.");
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
	
	
	
	
	private LocalResource createAppMasterJar() throws IOException {
		LocalResource appMasterJar = Records.newRecord(LocalResource.class);
		if (!appJarPath.isEmpty()) {
			appMasterJar.setType(LocalResourceType.FILE);
			Path jarPath = new Path(appJarPath);
			jarPath = FileSystem.get(conf).makeQualified(jarPath);
			appMasterJar.setResource(ConverterUtils.getYarnUrlFromPath(jarPath));
			appMasterJar.setTimestamp(appJarTimestamp);
			appMasterJar.setSize(appJarPathLen);
			appMasterJar.setVisibility(LocalResourceVisibility.PUBLIC);
		}
		return appMasterJar;
	}

	/**
	 * Launch container by create ContainerLaunchContext
	 *
	 * @param appMasterJar
	 * @param containerEnv
	 * @return
	 */
	private ContainerLaunchContext createContainerLaunchContext(LocalResource appMasterJar,
			Map<String, String> containerEnv) {
		
		GetIP getIP = new GetIP();//어플리케이션마스터 ip 가져옴
		String ip = getIP.getIP();
		
		PortCheck pc = new PortCheck();//포트번호 안쓰는거 2개 생성
		pc.available();
		
		YARN_AM_FILE_SEND_PORT = pc.YARN_AM_FILE_SEND_PORT;//안쓰는 포트 2개 할당
		YARN_AM_FILE_RECEIVE_PORT = pc.YARN_AM_FILE_RECEIVE_PORT;
		
		//System.out.println("ip/port : " + ip + " " + YARN_AM_FILE_SEND_PORT + " " + YARN_AM_FILE_RECEIVE_PORT);
		
		ContainerLaunchContext appContainer = Records.newRecord(ContainerLaunchContext.class);
		appContainer.setLocalResources(Collections.singletonMap(Constants.AM_JAR_NAME, appMasterJar));
		appContainer.setEnvironment(containerEnv);
		appContainer.setCommands(Collections
				.singletonList("$JAVA_HOME/bin/java" + " -Xmx256M" + " com.render.main.YarnClient" + " --Parameters "
						+ clientIP + " " + ip + " " + YARN_AM_FILE_SEND_PORT + " " + YARN_AM_FILE_RECEIVE_PORT + " 1>"
						+ ApplicationConstants.LOG_DIR_EXPANSION_VAR + "/stdout"
						+ " 2>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR + "/stderr"));
		// Parameter : 1) Client IP Address 2) Application Master IP Address

		return appContainer;
	}
	
	
	public void fileReceiver() {
		
		// 클라이언트로 부터 자료 수신
		String serverIp = clientIP;
		Socket socket = null;
		
		
		try {
			// 서버 연결
			socket = new Socket(serverIp, PORT);
			System.out.println("서버에 연결되었습니다.");
			
			// 파일 수신 작업 시작 및 데이터 파싱
			DataReceiver dr = new DataReceiver(socket);
			dr.receiveStart();
			
			
			id = dr.getID();
			basicList = dr.getBasicInfo();
			renderList = dr.getRenderInfo();
			resultFileName = dr.getResultFileName();
			
			fileName = dr.getFileName();
			
			
			
			for (int i = 0; i < fileName.size(); i++) {
			
				// 서버 연결
				socket = new Socket(serverIp, PORT);
				System.out.println("서버에 연결되었습니다.");
				
				
				// 파일 수신 작업 시작 및 데이터 파싱
				FileReceiver fr = new FileReceiver(socket);
				fr.receiveStart(id, fileName.get(i));
				
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		if (id.equals("")) {
			System.out.println("Parsing Error!!");
			System.exit(1);
		}

	}

	public void fileTransfer() {

		// 다운로드 서버로 결과 동영상 전송
		Socket socket = null;

		DataOutputStream dos;
		FileInputStream fis;
		BufferedInputStream bis;

		try {
			System.out.println("파일 전송 작업을 시작합니다.");

			socket = new Socket(WEB_SERVER_IP, WEB_SERVER_PORT);
			System.out.println("서버에 연결되었습니다.");

			dos = new DataOutputStream(socket.getOutputStream());

			// 파일 이름 전송
			String fName = "/home/hadoop/Render/" + id + "/resultTemp/result.mov";// 보낼
																					// 파일
			String receiveFileName = resultFileName + ".mov";// 받을 파일 이름

			// 데이터전송
			dos.writeUTF(id);
			System.out.printf("아이디를 전송하였습니다.\n");

			dos.writeUTF(receiveFileName);
			System.out.printf("파일 이름(%s)을 전송하였습니다.\n", fName);

			// 파일 내용을 읽으면서 전송
			File f = new File(fName);
			fis = new FileInputStream(f);
			bis = new BufferedInputStream(fis);

			int len;
			int size = 4096;
			byte[] data = new byte[size];
			while ((len = bis.read(data)) != -1) {
				dos.write(data, 0, len);
			}

			dos.flush();
			dos.close();
			bis.close();
			fis.close();
			System.out.println("파일 전송 작업을 완료하였습니다.");
			System.out.println("보낸 파일의 사이즈 : " + f.length());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {

		try {
			MyApplicationMaster appMaster = new MyApplicationMaster();
			LOG.info("Initializing MyApplicationMaster");
			boolean doRun = appMaster.init(args);
			if (!doRun) {
				System.exit(0);
			}
			appMaster.run();
		} catch (Throwable t) {
			LOG.fatal("Error running MyApplicationMaster", t);
			LogManager.shutdown();
			ExitUtil.terminate(1, t);
		}
	}

}
