#include <cv.h>
#include <highgui.h>
#include "com_render_jni_BW.h"

JNIEXPORT void JNICALL Java_com_render_jni_BW_bw(JNIEnv *env, jobject obj, jstring fileName){
	
	const char* str = env->GetStringUTFChars(fileName, 0);
	
	IplImage* img = cvLoadImage(str);
	IplImage* gray  = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
	
	cvCvtColor(img, gray, CV_BGR2GRAY);
	
	cvSaveImage(str, gray);
	
	cvReleaseImage(&gray);
	cvReleaseImage(&img);
	
}
