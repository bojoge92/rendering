#include <cv.h>
#include <highgui.h>
#include "com_render_jni_Resize.h"
JNIEXPORT void JNICALL Java_com_render_jni_Resize_resize
(JNIEnv *env, jobject obj, jstring jFileName, jint jWidth, jint jHeight){
	
	const char* fileName = env->GetStringUTFChars(jFileName, 0);
	int width = jWidth;
	int height = jHeight;
	
	IplImage* img = cvLoadImage(fileName, 1);

	IplImage* new_img = cvCreateImage(cvSize(width, height), img->depth, img->nChannels);
	cvResize(img, new_img);
	
	cvSaveImage(fileName, new_img);
	
	cvReleaseImage(&img);
	cvReleaseImage(&new_img);
	
}

