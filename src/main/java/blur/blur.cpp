
#include <cv.h>
#include <highgui.h>
#include <stdio.h>
#include "com_render_jni_Blur.h"

JNIEXPORT void JNICALL Java_com_render_jni_Blur_blur(JNIEnv *env, jobject obj, jstring fileName){
	
	const char* str = env->GetStringUTFChars(fileName, 0);
	
	IplImage* img = cvLoadImage(str, 1);
	IplImage* dst = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 3);
	cvSmooth(img, dst, CV_BLUR, 25, 25);
	
	cvSaveImage(str, dst);   //srcimg_R 이라는 IplImage를 저장 
	
	cvReleaseImage(&img);
	cvReleaseImage(&dst);
	
}

